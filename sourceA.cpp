#include <iostream>
#include <sstream>
#include "sqlite3.h"

using namespace std;

int main()
{
	int rc = 0;
	sqlite3 *db;
	char *zErrMsg = 0;

	rc = sqlite3_open("FirstPart.db", &db);

	if (rc)
	{
		cout << "can't open database :" << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
		system("pause");
		return(1);
	}

	std::stringstream s;

	s.str("create table people(id INTEGER PRIMARY KEY AUTOINCREMENT, name string);");
	sqlite3_exec(db, s.str().c_str(), nullptr, nullptr, &zErrMsg);

	

	s.str("insert into people(name) values('Boruto s dad');");
	sqlite3_exec(db, s.str().c_str(), nullptr, nullptr, &zErrMsg);
	
	s.str("insert into people(name) values('Sasuke');");
	sqlite3_exec(db, s.str().c_str(), nullptr, nullptr, &zErrMsg);
	
	s.str("insert into people(name) values('Sakura');");
	sqlite3_exec(db, s.str().c_str(), nullptr, nullptr, &zErrMsg);
	

	s.str("update people set name='Naruto' where name= 'Boruto s dad';");
	sqlite3_exec(db, s.str().c_str(), nullptr, nullptr, &zErrMsg);
	
	
	system("pause");
	return 0;
}