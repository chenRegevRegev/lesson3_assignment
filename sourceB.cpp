#include <iostream>
#include <sstream>
#include "sqlite3.h"

using namespace std;

bool balanceTransfer(int from, int to, int amount, sqlite3* db, char* zErrMsg);
int getBalace(void *notUse, int argc, char** argv, char ** azCol);
string toString(int num);
int strToInt(string str);

int balance = 0;

int main()
{
	int rc = 0;

	sqlite3 *db;
	char *zErrMsg = 0;

	rc = sqlite3_open("assign1.db", &db);

	if (rc)
	{
		cout << "can't open database :" << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
		system("pause");
		return(1);
	}

	
	cout<< "trasfer success: "<<(balanceTransfer(3, 1, 1000, db, zErrMsg)?"true":"false")<<endl;


	system("pause");
	return 0;
}
/*the funtion transfer from one acount to other
Input:
	from -the source acount
	to - dest acount
	amount -the amount to pass
	db - the database
	zErrMsg - error container
Output:
	transfer success-true, fail -false*/
bool balanceTransfer(int from, int to, int amount, sqlite3* db, char* zErrMsg)
{
	std::stringstream s;
	int balanceFrom = 0;
	int balanceTo = 0;

	s.str("select * FROM accounts where id = "+toString(from)+";");
	sqlite3_exec(db, s.str().c_str(), getBalace, nullptr, &zErrMsg);
	balanceFrom = balance;

	s.str("select * FROM accounts where id = " + toString(to) + ";");
	sqlite3_exec(db, s.str().c_str(), getBalace, nullptr, &zErrMsg);
	balanceTo = balance;

	s.str("begin transaction;");
	sqlite3_exec(db, s.str().c_str(), nullptr, nullptr, &zErrMsg);
	if (zErrMsg)
	{
		return false;
	}

	s.str("update accounts set balance=" + toString(balanceFrom - amount) + " where id =" + toString(from) + ";");
	sqlite3_exec(db, s.str().c_str(), nullptr, nullptr, &zErrMsg);
	if (zErrMsg)
	{
		return false;
	}

	s.str("update accounts set balance=" + toString(balanceTo + amount) + " where id =" + toString(to) + ";");
	sqlite3_exec(db, s.str().c_str(), nullptr, nullptr, &zErrMsg);
	if (zErrMsg)
	{
		return false;
	}

	s.str("commit;");
	sqlite3_exec(db, s.str().c_str(), nullptr, nullptr, &zErrMsg);
	
	return zErrMsg ? false : true;
}

/*the function return the balance of acount from sql returned value(callback function)*/
int getBalace(void *unUse, int argc, char** argv, char ** azCol)
{
	string temp = "";
	
	for (int i = 0; i < argc; i++)
	{
		temp = azCol[i];
		if (temp == "balance")
		{
			balance = strToInt(argv[i]);
		}
	}
	return 0;
}

/*
the function convert int to string
Input:
	num- the int
Output:
	str - the converted int
*/
string toString(int num)
{
	string str = "";
	string temp = "";
	
	str = num ? "" : "0";
	int num2 = num < 0 ? -num : num;

	while (num2 != 0)
	{
		temp += (num2 % 10) + '0';
		str.insert(0,temp);
		num2 /= 10;
		temp = "";
	}
	temp =  num < 0 ? "-" : "";
	str.insert(0, temp);
	return str;
}

/*
the function convert string to int
Input:
	str - the string
Output:
	num- the converted int
*/
int strToInt(string str)
{
	int num = 0;


	for (int i = str[0]=='-'?1:0; i < str.length(); i++)
	{
		num *= 10;
		num += str[i]-'0';
	}

	num = str[0] == '-' ? -num : num;
	return num;
}